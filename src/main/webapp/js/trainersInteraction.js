let buttonId = "formButton";
let formId = "insertTrainer";
const itemsPerPage = 5;
let currentPage = 1;
let items


function showForm() {

    const form = document.getElementById(formId);
    form.innerHTML = `
    <form id="newTrainerForm" onsubmit="createTrainer()">
        <br>    
        <div>
            <label for="fname">Trainer name</label>
            <input type="text" id="fname" name="fname" required>
        </div>
        
        <br>
        
        <div>
            <label for="img">Trainer's picture</label>
            <input type="text" id="img" picturePath="img" placeholder="Enter url" required>
        </div>
        
        <br>
        
        <div>
           <button type="submit" class="btn btn-success" onsubmit="createTrainer()">Submit</button>
        </div>
        
     </form>
    `

}

function showUpdateForm() {

    const form = document.getElementById(formId)
    form.innerHTML = `
    <form id="updateTrainer" onsubmit = "updateTrainer()">
        <br>
        <div>
        <label for="id">Trainer ID</label>
            <input type="text" id="trainerID" trainerID="id" required>
        </div>
        
        <br>
        <dib>
            <label for="newName">New Name</label>
            <input type="text" id="nName" newName="newName">
        </dib>


        <br>
        <dib>
        <label for="img">Trainer's new picture</label>
            <input type="text" id="img" picturePath="img" placeholder="Enter url">
        </div>
        
        
         <div>
           <button type="submit" class="btn btn-success" onsubmit="updateTrainer()"">Update</button>
        </div>
        
    </form>
    `

}


function updateTrainer() {

    const newName = document.getElementById("nName").value;
    const idOfTrainer = document.getElementById("trainerID").value;
    const newImage = document.getElementById("img").value;

    const updatedTrainer = {
        name: newName,
        id: parseInt(idOfTrainer, 10),
        profileUrl: newImage
    }
    fetch(`/pokemon/api/trainers/${idOfTrainer}`, {
        method: "PUT",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(updatedTrainer)
    }).then(res => {
        console.log('Raw Response:', res);
        return res.json();
    })
        .then(data => {
            data.map(resource => `${updateRow(resource)}`)
        })
        .catch(err => {
            console.error(`Unable to update the pokemon trainer : ${err.status}`)
        })

}

function updateRow(res) {
    const row = document.getElementById(`${res.id}_row`)
    if(row) {
        row.innerHTML = `
        <th scope="row">${res.id}</th>
        <td>${res.name}</td>
        <td>${res.lastUpDate}</td>
        `
    } else {
        console.error(`Row with the ${res.id} was not found.`)
    }
}


function createTrainer() {
    const name = document.getElementById("fname").value;
    const image = document.getElementById("img").value;

    const newTrainer = {
        name: name,
        profileUrl: image
    }

    fetch('/pokemon/api/trainers/', {
        method : 'POST',
        headers: { 'Content-Type' : 'application/json' },
        body: JSON.stringify(newTrainer)
    })
        .then(res => res.json())
        .then(data => {
            data.map(resource => `${pokemonTrainerToRow(resource)}`).join("\n")
        })
        .catch(err => {
            console.error(`Unable to create trainer : ${err.status}`)
            console.error(err);
        });


}

function toggleUpdateButton() {

    const tableBody = document.querySelector("#tableDiv tbody");
    const updateButton = document.getElementById("updateButton");

    if(tableBody && tableBody.rows.length > 0) {
        updateButton.style.display = "inline-block"; //Show the button
    } else {
        updateButton.style.display = "none"; //Hide the button
    }
}

function updatePagination() {
    const table = document.querySelector(".col-8");
    items = Array.from(table.getElementsByTagName('tr')).slice(1);
}

function showPage(page) {
    const startIndex = (page - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;

    items.forEach((item, index) => {
        item.classList.toggle('d-none', index < startIndex || index >= endIndex);
        updateActiveButtonStates();
    })
}

function createPageButtons() {
    const totalPages = Math.ceil(items.length/itemsPerPage)
    const paginationContainer = document.querySelector(".pagination")

    for(let i = 1; i <= totalPages; i++) {
        const pageItem = document.createElement('li')
        pageItem.classList.add('page-item')
        // Assign a unique ID to the <li> element
        pageItem.id = `page-item-${i}`;
        const pageButton = document.createElement('a')
        pageButton.classList.add('page-link')
        pageButton.href = '#'; // Prevents the anchor from navigating
        pageButton.textContent = `${i}`

        pageItem.appendChild(pageButton);
        paginationContainer.appendChild(pageItem)

        pageButton.addEventListener('click', function (event) {
            event.preventDefault()
            currentPage = i;
            showPage(i);
        } )
    }
}

function updateActiveButtonStates() {
    const buttons = document.querySelectorAll('.pagination li')
    buttons.forEach((button, index) => {
        if ((index + 1) === currentPage) {
            button.classList.add('active');
        } else {
            button.classList.remove('active');
        }
    })
}

function deletePaginationButton() {
    const buttons = document.querySelector(".pagination")
    if( currentPage !==  Math.ceil(items.length/itemsPerPage)) {
        const itemToRemove = document.getElementById(`page-item-${currentPage}`)
        if (buttons && itemToRemove) {
            buttons.removeChild(itemToRemove)
            currentPage--;
            showPage(currentPage)
        }

    }
}

