package nl.utwente.mod4.pokemon.routes;

import nl.utwente.mod4.pokemon.model.PokemonEntity;

import java.util.*;

public class PokemonHelperClass {

    public static Map<Integer, PokemonEntity> pokemonMap = new HashMap<Integer, PokemonEntity>();

    static {
        pokemonMap.put(0, new PokemonEntity("Pikachu", 513, 1, 0));
        pokemonMap.put(1, new PokemonEntity("Pikipek", 514, 2, 1));
    }

    public static PokemonEntity getPokemon(int id) {
        return pokemonMap.get(id);
    }

    public static void deletePokemon(int id) {
        pokemonMap.remove(id);
    }

    public static void createPokemon(PokemonEntity pokemon) {
        if(pokemon.id == 0) {
            pokemon.setId(pokemonMap.size());
            pokemonMap.put(pokemonMap.size(), pokemon);
        }
        pokemonMap.put(pokemon.id, pokemon);
    }

    public static PokemonEntity[] getPokemons(String sortBy) {
        if (sortBy != null && sortBy.equals("name")) {
            return getSortedPokemons();
        }
        return pokemonMap.values().toArray(new PokemonEntity[0]);
    }

    public static PokemonEntity[] getSortedPokemons() {
        ArrayList<PokemonEntity> pokemonsList = new ArrayList<>(pokemonMap.values());
        pokemonsList.sort(Comparator.comparing(o -> o.name));
        return pokemonsList.toArray(new PokemonEntity[0]);
    }



}
