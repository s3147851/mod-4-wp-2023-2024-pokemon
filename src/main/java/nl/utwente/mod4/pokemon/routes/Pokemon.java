package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import java.util.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nl.utwente.mod4.pokemon.model.PokemonEntity;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

@Path("/pokemon")
public class Pokemon {

    public int counter = 0;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonEntity> getAllPokemon(@QueryParam("sortBy") String sortBy) {
        PokemonEntity[] pokemons = PokemonHelperClass.getPokemons(sortBy);
        return new ResourceCollection<>(pokemons, 1, 1, 1);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonEntity> getPokemonById(@PathParam("id") int id) {

            PokemonEntity requestedPokemon = PokemonHelperClass.getPokemon(id);
            if (requestedPokemon == null) {
                throw new NotFoundException();
            }
            return new ResourceCollection<>(new PokemonEntity[]{requestedPokemon}, 1, 1, 1);

    }

    @DELETE
    @Path("/{id}")
    public void deletePokemonById(@PathParam("id") int id) {
        PokemonEntity pokemonToDelete = PokemonHelperClass.getPokemon(id);
        if (pokemonToDelete == null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        } else {
            PokemonHelperClass.deletePokemon(id);
        }
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonEntity> updatePokemonById(@PathParam("id") int id, PokemonEntity toReplace) {
        PokemonEntity pokemonToUpdate = PokemonHelperClass.getPokemon(id);
        if (pokemonToUpdate == null || toReplace.id != pokemonToUpdate.id) {
            throw new NotFoundException();
        } else {
            PokemonHelperClass.createPokemon(toReplace);
        }
        return new ResourceCollection<>(new PokemonEntity[]{PokemonHelperClass.getPokemon(id)}, 1, 1, 1);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<PokemonEntity> addPokemon(PokemonEntity toAdd) {

        if(toAdd.id != 0 || toAdd.created != null || toAdd.lastUpDate != null ) {
            throw new BadRequestException();
        }

        PokemonHelperClass.createPokemon(toAdd);
        return new ResourceCollection<>(new PokemonEntity[]{PokemonHelperClass.getPokemon(toAdd.id)}, 1, 1, 1);
    }

}
