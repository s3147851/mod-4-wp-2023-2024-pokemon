package nl.utwente.mod4.pokemon.model;

public class PokemonEntity extends NamedEntity {

    public int id;
    public String created;
    public String lastUpDate;
    public String name;
    public int pokemonType;
    public int trainerId;

    public PokemonEntity(String name, int pokemonType, int trainerId, int id) {
        super();
        this.name = name;
        this.pokemonType = pokemonType;
        this.trainerId = trainerId;
        this.id = id;
    }

    public PokemonEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastUpDate() {
        return lastUpDate;
    }

    public void setLastUpDate(String lastUpDate) {
        this.lastUpDate = lastUpDate;
    }

    public int getPokemonType() {
        return pokemonType;
    }

    public void setPokemonType(int pokemonType) {
        this.pokemonType = pokemonType;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }
}
